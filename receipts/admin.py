from django.contrib import admin
from receipts.models import ExpenseCategory, Receipt, Account

# Register your models here.


@admin.register(ExpenseCategory)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "id",
    )


@admin.register(Account)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ("name", "number", "owner", "id")


@admin.register(Receipt)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
        "id",
    )
